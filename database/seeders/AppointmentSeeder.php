<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 3; $i++) {
            $datetime = Carbon::parse('2021-04-30 08:00');
            for ($j = 1; $j <= 200; $j++) {
                DB::table('appointments')->insert([
                    'doctor_id' => $i,
                    'day_of_week' => $datetime->dayOfWeekIso,
                    'slot_time' => $datetime->format('H:i'),
                    'slot_date' => $datetime->format('Y-m-d'),
                    'status' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                $datetime->addMinutes(30);

                if ($datetime->format('H:i') >= '17:00') {
                    $datetime = Carbon::parse($datetime->addDay()->format('Y-m-d') . '08:00');
                }
            }
        }
    }
}
