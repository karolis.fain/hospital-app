<?php

namespace Database\Seeders;

use App\Models\Drug;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DrugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $drugs = [];

        for ($i = 0; $i < 25000; $i++) {
            $drugs[] = [
                'name' => Str::random(12) . ' ' . rand(1, 3200),
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ];
        }

        $chunks = array_chunk($drugs, 5000);
        foreach ($chunks as $chunk) {
            Drug::insert($chunk);
        }
    }
}
