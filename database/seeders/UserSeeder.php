<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Doctor1',
            'email' => 'doctor1@gmail.com',
            'role_id' => '1',
            'password' => bcrypt('test1234'),
        ]);
        DB::table('users')->insert([
            'name' => 'Doctor2',
            'email' => 'doctor2@gmail.com',
            'role_id' => '1',
            'password' => bcrypt('test1234'),
        ]);
        DB::table('users')->insert([
            'name' => 'Doctor3',
            'email' => 'doctor3@gmail.com',
            'role_id' => '1',
            'password' => bcrypt('test1234'),
        ]);
        DB::table('users')->insert([
            'name' => 'Receptionist',
            'email' => 'receptionist@gmail.com',
            'role_id' => '2',
            'password' => bcrypt('test1234'),
        ]);
        DB::table('users')->insert([
            'name' => 'Patient',
            'email' => 'patient@gmail.com',
            'role_id' => '3',
            'password' => bcrypt('test1234'),
        ]);
    }
}
