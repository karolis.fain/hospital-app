export default Object.freeze({
    PATIENTS_URL: "http://localhost:8000/patients",
    DOCTORS_URL: "http://localhost:8000/appointments/doctors",

    PRESCRIPTIONS_URL: "http://localhost:8000/prescriptions",
    DOCTOR_PRESCRIPTIONS_URL: "http://localhost:8000/prescriptions/doctor",
    DRUGS_URL: "http://localhost:8000/prescriptions/drugs",

    APPOINTMENT_URL: "http://localhost:8000/appointments",
    APPOINTMENT_CREATE_URL: "http://localhost:8000/appointments/create",
    APPOINTMENT_RESERVE_URL: "http://localhost:8000/appointments/reserve",
    APPOINTMENT_CANCEL_RESERVE_URL: "http://localhost:8000/appointments/cancel",
    APPOINTMENT_EDIT_URL: "http://localhost:8000/appointments/edit",
    TAKEN_APPOINTMENTS_DATES_URL: "http://localhost:8000/appointments/taken",
    DOCTOR_APPOINTMENTS_URL: "http://localhost:8000/patients/doctor/page=",
})
