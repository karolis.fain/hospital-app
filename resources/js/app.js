/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap')

import Vue from 'vue'

import AppointmentComponent from './components/appointments/AppointmentComponent'
import CreateAppointmentComponent from "./components/appointments/CreateAppointmentComponent";

import PatientComponent from './components/patients/PatientComponent'

import PrescriptionComponent from './components/prescriptions/PrescriptionComponent'
import CreatePrescriptionComponent from "./components/prescriptions/CreatePrescriptionComponent";

const app = new Vue({
    el: '#app',
    components: {
        AppointmentComponent,
        CreateAppointmentComponent,
        PatientComponent,
        PrescriptionComponent,
        CreatePrescriptionComponent,
    },
});
