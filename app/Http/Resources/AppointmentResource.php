<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'doctor' => $this->doctor->email,
            'user' => $this->user->email,
            'day_of_week' => $this->day_of_week,
            'date' => $this->slot_date . ' ' . $this->slot_time,
            'status' => $this->status,
            'updated_at' => $this->updated_at->format('Y-m-d H:i'),
        ];
    }
}
