<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\AppointmentRetriever;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class PatientController
{
    /**
     * @var AppointmentRetriever $appointmentRetriever
     */
    private AppointmentRetriever $appointmentRetriever;

    /**
     * ReceptionistController constructor.
     * @param AppointmentRetriever $appointmentRetriever
     */
    public function __construct(AppointmentRetriever $appointmentRetriever)
    {
        $this->appointmentRetriever = $appointmentRetriever;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('patients.list');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function doctorAppointments(): JsonResponse
    {
        $doctorPatients = $this->appointmentRetriever->getDoctorAppointments(Auth::user()->id);

        return response()->json($doctorPatients);
    }

    /**
     * @param string $search
     * @param User $user
     * @return JsonResponse
     */
    public function patients(string $search, User $user): JsonResponse
    {
        $patients = $user->query()
            ->where('role_id', Config::get('roles.patient'))
            ->where('email', 'LIKE', "%$search%")
            ->limit(20)
            ->get();

        return response()->json($patients);
    }

}
