<?php

namespace App\Http\Controllers;

use App\Http\Requests\AppointmentRequest;
use App\Http\Resources\AppointmentResource;
use App\Http\Resources\TimesResource;
use App\Models\Appointment;
use App\Models\User;
use App\Services\AppointmentRetriever;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;

class AppointmentController extends Controller
{
    /**
     * @var AppointmentRetriever $appointmentRetriever
     */
    private AppointmentRetriever $appointmentRetriever;

    /**
     * ReceptionistController constructor.
     * @param AppointmentRetriever $appointmentRetriever
     */
    public function __construct(AppointmentRetriever $appointmentRetriever)
    {
        $this->appointmentRetriever = $appointmentRetriever;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('appointments.list');
    }

    /**
     * @return JsonResponse
     */
    public function takenAppointments(): JsonResponse
    {
        $takenAppointments = $this->appointmentRetriever->getTakenAppointments();

        return response()->json(AppointmentResource::collection($takenAppointments));
    }

    /**
     * @return JsonResponse
     */
    public function availableAppointments(): JsonResponse
    {
        $takenAppointments = $this->appointmentRetriever->getTakenAppointments();

        return response()->json(AppointmentResource::collection($takenAppointments));
    }

    /**
     * @param int $doctorId
     * @return JsonResponse
     */
    public function getTakenAppointmentsDates(int $doctorId): JsonResponse
    {
        $takenDates = $this->appointmentRetriever->getTakenAppointmentsDates($doctorId);

        return response()->json(TimesResource::collection($takenDates));
    }


    /**
     * @param string $search
     * @param User $user
     * @return JsonResponse
     */
    public function doctors(string $search, User $user): JsonResponse
    {
        $doctors = $user->query()
            ->where('role_id', Config::get('roles.doctor'))
            ->where('email', 'LIKE', "%$search%")
            ->get();

        return response()->json($doctors);
    }

    /**
     * @param AppointmentRequest $request
     * @return JsonResponse
     */
    public function reserve(AppointmentRequest $request): JsonResponse
    {
        $validData = $request->validated();
        $appointment = $this->appointmentRetriever->getAvailableAppointment($validData);

        if ($appointment) {
            $appointment->update([
                'status' => Config::get('constants.reserved'),
                'user_id' => $validData['user_id']
            ]);

            return response()->json(204);
        }

        return response()->json('Already reserved time!', 400);
    }

    /**
     * @param AppointmentRequest $request
     * @return JsonResponse
     */
    public function cancel(AppointmentRequest $request): JsonResponse
    {
        $validData = $request->validated();
        $appointment = $this->appointmentRetriever->getReservedAppointment($validData);

        if ($appointment) {
            $appointment->update([
                'status' => Config::get('constants.available'),
                'user_id' => null
            ]);

            return response()->json(204);
        }

        return response()->json(204);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('appointments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AppointmentRequest $request
     * @return JsonResponse
     */
    public function store(AppointmentRequest $request): JsonResponse
    {
        $validData = $request->validated();
        $validData['slot_date'] = Carbon::parse($validData['date'])->format('Y-m-d');
        $validData['slot_time'] = Carbon::parse($validData['date'])->format('H:i:s');;
        $appointment = $this->appointmentRetriever->getUserReserOrAvailableAppointment($validData);

        if ($appointment) {
            $appointment->update([
                'status' => Config::get('constants.taken'),
                'slot_date' => $validData['slot_date'],
                'slot_time' => $validData['slot_time'],
                'description' => $validData['description']
            ]);

            return response()->json(204);
        }

        return response()->json('Time is reserved or taken!', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Appointment $appointment
     * @return JsonResponse
     */
    public function destroy(Appointment $appointment): JsonResponse
    {
        $appointment->update([
            'status' => Config::get('constants.available'),
            'description' => null,
            'user_id' => null
        ]);

        return response()->json('Successfully removed!');
    }

}
