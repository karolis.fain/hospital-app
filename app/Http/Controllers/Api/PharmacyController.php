<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PharmacyRequest;
use App\Services\PrescriptionRetriever;
use Illuminate\Http\JsonResponse;

class PharmacyController extends Controller
{
    /**
     * @param PharmacyRequest $request
     * @param PrescriptionRetriever $prescriptionRetriever
     * @return JsonResponse
     */
    public function patientPrescriptions(PharmacyRequest $request,
                                         PrescriptionRetriever $prescriptionRetriever): JsonResponse
    {
        return response()->json($prescriptionRetriever->getPrescriptionsByEmail($request->validated()['email']));
    }
}
