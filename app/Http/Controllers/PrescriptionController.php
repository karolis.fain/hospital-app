<?php

namespace App\Http\Controllers;

use App\Http\Requests\PrescriptionRequest;
use App\Services\PrescriptionRetriever;
use App\Services\PrescriptionService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class PrescriptionController extends Controller
{
    /**
     * @var PrescriptionRetriever $prescriptionRetriever
     */
    private PrescriptionRetriever $prescriptionRetriever;

    /**
     * @var PrescriptionService $prescriptionService
     */
    private PrescriptionService $prescriptionService;

    /**
     * ReceptionistController constructor.
     * @param PrescriptionRetriever $prescriptionRetriever
     * @param PrescriptionService $prescriptionService
     */
    public function __construct(PrescriptionRetriever $prescriptionRetriever,
                                PrescriptionService $prescriptionService)
    {
        $this->prescriptionRetriever = $prescriptionRetriever;
        $this->prescriptionService = $prescriptionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('prescriptions.list');
    }

    /**
     * @param string $search
     * @return JsonResponse
     */
    public function drugs(string $search): JsonResponse
    {
        return response()->json($this->prescriptionRetriever->getDrugs($search));
    }

    /**
     * @return JsonResponse
     */
    public function prescriptions(): JsonResponse
    {
        return response()->json($this->prescriptionRetriever->getDoctorPrescriptions(Auth::user()->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('prescriptions.create');
    }

    /**
     * @param PrescriptionRequest $request
     * @return JsonResponse
     */
    public function store(PrescriptionRequest $request): JsonResponse
    {
        return response()->json($this->prescriptionService->createPrescription($request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        if ($this->prescriptionService->cancelPrescription($id)) {
            response()->json('Successfully removed!');
        }

        return response()->json([
            'error' => 'Cannot delete prescriptions older than 1 hour!'
        ], 422);
    }

}
