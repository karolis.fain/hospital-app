<?php

namespace App\Services;

use App\Models\Drug;
use App\Models\Prescription;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class PrescriptionRetriever
{
    /**
     * @var Prescription $prescription
     */
    private Prescription $prescription;

    /**
     * @var Drug $drug
     */
    private Drug $drug;

    /**
     * PrescriptionRetriever constructor.
     * @param Prescription $prescription
     * @param Drug $drug
     */
    public function __construct(Prescription $prescription, Drug $drug)
    {
        $this->prescription = $prescription;
        $this->drug = $drug;
    }

    /**
     * @param string $email
     * @return Collection
     */
    public function getPrescriptionsByEmail(string $email): Collection
    {
        return $this->prescription->query()
            ->with(['user' => function($q) use($email) {
                $q->where('email', $email);
            }, 'drug'])
            ->orderBy('created_at')
            ->get();
    }

    /**
     * @param int $doctorId
     * @return LengthAwarePaginator
     */
    public function getDoctorPrescriptions(int $doctorId): LengthAwarePaginator
    {
        return $this->prescription->query()
            ->with('drug', 'user')
            ->where('doctor_id', $doctorId)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
    }

    public function getDrugs($search): Collection
    {
        return $this->drug->query()
            ->where('name', 'LIKE', "%$search%")
            ->limit(20)
            ->get();
    }

}
