<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Appointment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Config;

class AppointmentRetriever
{
    /**
     * @var Appointment $appointment
     */
    private Appointment $appointment;

    /**
     * Appointment constructor.
     * @param Appointment $appointment
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * @param array $validData
     * @return Appointment|null
     */
    public function getUserReserOrAvailableAppointment(array $validData)
    {
        return $this->appointment->query()
            ->where('doctor_id', $validData['doctor_id'])
            ->where('slot_date', $validData['slot_date'])
            ->where('slot_time', $validData['slot_time'])
            ->where(function ($query) use ($validData) {
                $query->where('status', Config::get('constants.reserved'))
                    ->where('user_id', $validData['user_id'])
                    ->orWhere('status', Config::get('constants.available'));
            })
            ->first();
    }

    /**
     * @param array $reqData
     * @return Appointment|null
     */
    public function getAvailableAppointment(array $reqData)
    {
        return $this->appointment->query()
            ->where('doctor_id', $reqData['doctor_id'])
            ->where('slot_date', $this->getDateString($reqData['date'], 'Y-m-d'))
            ->where('slot_time', $this->getDateString($reqData['date'], 'H:i:s'))
            ->where('status', Config::get('constants.available'))
            ->first();
    }

    /**
     * @param array $reqData
     * @return Appointment|null
     */
    public function getReservedAppointment(array $reqData)
    {
        return $this->appointment->query()
            ->where('doctor_id', $reqData['doctor_id'])
            ->where('slot_date', $this->getDateString($reqData['date'], 'Y-m-d'))
            ->where('slot_time', $this->getDateString($reqData['date'], 'H:i:s'))
            ->where('status', Config::get('constants.reserved'))
            ->where('user_id', $reqData['user_id'])
            ->first();
    }

    /**
     * @return Collection
     */
    public function getTakenAppointments(): Collection
    {
        return $this->appointment->query()
            ->where('status', Config::get('constants.taken'))
            ->with('user', 'doctor')
            ->get();
    }

    /**
     * @param int $doctorId
     * @return Collection
     */
    public function getTakenAppointmentsDates(int $doctorId): Collection
    {
        return $this->appointment->query()
            ->select('slot_date', 'slot_time')
            ->where('doctor_id', $doctorId)
            ->where(function ($query) {
                $query->where('status', Config::get('constants.taken'))
                    ->orWhere('status', Config::get('constants.reserved'));
            })
            ->get();
    }

    /**
     * @param int $doctorId
     * @return LengthAwarePaginator
     */
    public function getDoctorAppointments(int $doctorId): LengthAwarePaginator
    {
        return $this->appointment->query()
            ->where('doctor_id', $doctorId)
            ->where('status', Config::get('constants.taken'))
            ->where('slot_date', '>=', Carbon::today())
            ->with('user')
            ->orderBy('updated_at')
            ->paginate(20);
    }

    /**
     * @param string $date
     * @param string $format
     * @return string
     */
    private function getDateString(string $date, string $format): string
    {
        return Carbon::parse($date)->format($format);
    }

}
