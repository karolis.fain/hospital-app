<?php

namespace App\Services;

use App\Models\Prescription;
use App\Models\User;
use App\Notifications\PrescriptionEmail;
use Illuminate\Support\Facades\Notification;

class PrescriptionService
{
    /**
     * @var Prescription $prescription
     */
    private Prescription $prescription;

    /**
     * PrescriptionService constructor.
     * @param Prescription $prescription
     */
    public function __construct(Prescription $prescription)
    {
        $this->prescription = $prescription;
    }

    /**
     * @param array $attributes
     * @return Prescription
     */
    public function createPrescription(array $attributes): Prescription
    {
        Notification::route('mail', User::find($attributes['user_id'])->email)
            ->notify(new PrescriptionEmail($attributes['description']));

        return $this->prescription->create($attributes);
    }

    /**
     * @param int $prescriptionId
     * @return int
     */
    public function cancelPrescription(int $prescriptionId): int
    {
        return $this->prescription->query()
            ->where('id', $prescriptionId)
            ->whereRaw('DATE_ADD(created_at, INTERVAL +1 HOUR) >= NOW()')
            ->delete();
    }
}
