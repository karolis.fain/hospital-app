<?php

use App\Http\Controllers\Api\PatientController;
use App\Http\Controllers\Api\PharmacyController;
use App\Http\Controllers\Api\PrescriptionController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Api\AppointmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/sanctum/token', [LoginController::class, 'loginSanctum']);

Route::group(['middleware' => 'auth:sanctum'], function() {

    Route::post('/pharmacy', [PharmacyController::class, 'patientPrescriptions']);
});
