<?php

use App\Http\Controllers\PatientController;
use App\Http\Controllers\PrescriptionController;
use App\Http\Controllers\AppointmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('layouts.app');
    });

    Route::group(['middleware' => 'role:receptionist', 'prefix' => 'appointments'], function () {

        Route::resource('/', AppointmentController::class)
            ->only(['index', 'create', 'destroy'])
            ->parameters([
                '' => 'appointment'
            ]);

        Route::get('/available', [AppointmentController::class, 'availableAppointments']);
        Route::get('/taken', [AppointmentController::class, 'takenAppointments']);
        Route::get('/taken/{id}', [AppointmentController::class, 'getTakenAppointmentsDates']);
        Route::get('/doctors/{search}', [AppointmentController::class, 'doctors']);

        Route::put('/', [AppointmentController::class, 'store']);
        Route::put('/cancel', [AppointmentController::class, 'cancel']);
        Route::put('/reserve', [AppointmentController::class, 'reserve']);
    });

    Route::group(['prefix' => 'patients'], function () {

        Route::get('/', [PatientController::class, 'index'])
            ->middleware(['middleware' => 'role:doctor']);

        Route::get('/doctor/{page?}', [PatientController::class, 'doctorAppointments'])
            ->middleware(['middleware' => 'role:doctor']);

        Route::get('/{search}', [PatientController::class, 'patients'])
            ->middleware(['middleware' => 'role:doctor,receptionist']);
    });

    Route::group(['middleware' => 'role:doctor', 'prefix' => 'prescriptions'], function () {

        Route::resource('/', PrescriptionController::class)
            ->only(['index', 'create', 'store',  'destroy'])
            ->parameters([
                '' => 'id'
            ]);

        Route::get('/doctor', [PrescriptionController::class, 'prescriptions']);

        Route::get('/drugs/{search}', [PrescriptionController::class, 'drugs']);
    });
});
